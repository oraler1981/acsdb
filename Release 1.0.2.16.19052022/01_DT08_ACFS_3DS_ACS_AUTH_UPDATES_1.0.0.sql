/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/

set time on
set timing on
set echo on
set head off

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';

REM*************************
REM* Change the spool name
REM*************************

col DATASPOOL noprint new_value NOME_REPORT

select '01_DT08_ACFS_3DS_ACS_AUTH_UPDATES_1.0.0_MEPS_'||to_char(CURRENT_DATE ,'YYYYMMDDHH24MISS')     DATASPOOL
  from dual ;
spool       &&NOME_REPORT..log
SELECT user FROM dual;

SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;

set serveroutput on

-------------------------------------
---          Update schema        ---
-------------------------------------

---              TR3E              ---

CREATE OR REPLACE EDITIONABLE SYNONYM ACS_AUTH_OWN.TR3E FOR ACS_OWN.TR3E;

-------------------------------------
---             Commit            ---
-------------------------------------

-- COMMIT

-------------------------------------
---              End              ---
-------------------------------------

spool off