/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/

set time on
set timing on
set echo on
set head off

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';

REM*************************
REM* Change the spool name
REM*************************

col DATASPOOL noprint new_value NOME_REPORT

select '02_DT08_ACFS_3DS_ACS_UPDATES_1.0.0_MEPS'||to_char(CURRENT_DATE ,'YYYYMMDDHH24MISS')     DATASPOOL
  from dual ;

spool       &&NOME_REPORT..log
SELECT user FROM dual;

SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;

set serveroutput on

-------------------------------------
---          Update schema        ---
-------------------------------------

---              TR3              ---
ALTER TABLE ACS_OWN.TR3 ADD BROWSERIP VARCHAR2(20);
COMMENT ON COLUMN ACS_OWN.TR3.BROWSERIP IS 'Brw ip address';
ALTER TABLE ACS_OWN.TR3 ADD BROWSERUSERAGENT VARCHAR2(255);
COMMENT ON COLUMN ACS_OWN.TR3.BROWSERUSERAGENT IS 'Brw user agent';
ALTER TABLE ACS_OWN.TR3 ADD PWD VARCHAR2(64);
COMMENT ON COLUMN ACS_OWN.TR3.PWD IS 'Single transaction password from 3part services';
ALTER TABLE ACS_OWN.TR3 MODIFY PSD2_EXEMPTION VARCHAR2(1024);

---         CERTIFICATES          ---

ALTER TABLE ACS_OWN.CRT ADD CSR CLOB DEFAULT NULL;
COMMENT ON COLUMN ACS_OWN.CRT.CSR IS 'Certificate CSR';
ALTER TABLE ACS_OWN.CRT ADD EXPIRATION_DATE DATE DEFAULT NULL;
COMMENT ON COLUMN ACS_OWN.CRT.EXPIRATION_DATE IS 'Certificate expiration date';

---         TR3E          ---

GRANT SELECT ON ACS_OWN.TR3E to ACS_AUTH_OWN;

-------------------------------------
---             Commit            ---
-------------------------------------

-- COMMIT

-------------------------------------
---              End              ---
-------------------------------------

spool off