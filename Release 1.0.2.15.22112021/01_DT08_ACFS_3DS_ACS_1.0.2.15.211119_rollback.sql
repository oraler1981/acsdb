/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/
set time on
set timing on
set echo on
set head off
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
---ACCEPT USER_OWNER PROMPT 'INSERT SCHEMA OWNER ' DEFAULT ACS20 ; USER_OWNER PROMPT 'INSERT SCHEMA OWNER ' DEFAULT ACS20 ;
---ACCEPT TBS_DATA PROMPT 'INSERT TABLSPACE NAME FOR DATA ' DEFAULT USERS ; TBS_DATA PROMPT 'INSERT TABLSPACE NAME FOR DATA ' DEFAULT USERS ;
---ACCEPT TBS_INDX PROMPT 'INSERT TABLSPACE NAME FOR INDEXES ' DEFAULT INDX; TBS_INDX PROMPT 'INSERT TABLSPACE NAME FOR INDEXES ' DEFAULT INDX;
REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';
REM*************************
REM* Change the spool name
REM*************************
col DATASPOOL noprint new_value NOME_REPORT
select '01_DT08_ACFS_3DS_ACS_1.0.2.15.211119_rollback_NEXI_'||to_char(CURRENT_DATE ,'YYYYMMDDHH24MISS')     DATASPOOL
  from dual ;
---spool       &&NOME_REPORT..log       &&NOME_REPORT..log
SELECT user FROM dual;
SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;
set serveroutput on
------------------------------------
--- Aggiornamento schema DB CORE --- 
------------------------------------

alter table ACS_OWN.TR3 drop column DS_NOTIFICATION;
 
----------------------------------
--- Aggiornamento dati DB CORE --- 
----------------------------------
--commit;
------------
--- Fine --- 
------------
---spool off off
SPOOL OFF;
