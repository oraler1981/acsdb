/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/

set time on
set timing on
set echo on
set head off

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';

REM*************************
REM* Change the spool name
REM*************************

col DATASPOOL noprint new_value NOME_REPORT

select '03_DT08_ACFS_3DS_ACS_WEB_WEB_TABLES_1.0.0_rollback'||to_char(CURRENT_DATE ,'YYYYMMDDHH24MISS')     DATASPOOL
  from dual ;
spool       &&NOME_REPORT..log
SELECT user FROM dual;

SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;

set serveroutput on

-------------------------------------
---          Update schema        ---
-------------------------------------

DROP TABLE ACS_OWN.WEB_USERS_INFO CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_USERS_PROFILES CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_PROFILES_PAGES CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_USERS CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_PROFILES CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_PAGES CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_FILES CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_TOKEN CASCADE CONSTRAINTS
/
DROP TABLE ACS_OWN.WEB_CONSOLE_LOG CASCADE CONSTRAINTS
/

-------------------------------------
---             Commit            ---
-------------------------------------

-- COMMIT

-------------------------------------
---              End              ---
-------------------------------------

spool off