/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/

set time on
set timing on
set echo on
set head off

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';

REM*************************
REM* Change the spool name
REM*************************

col DATASPOOL noprint new_value NOME_REPORT

select '03_DT08_ACFS_3DS_ACS_WEB_WEB_TABLES_1.0.0_MEPS'||to_char(CURRENT_DATE ,'YYYYMMDDHH24MISS')     DATASPOOL
  from dual ;

spool       &&NOME_REPORT..log
SELECT user FROM dual;

SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;

set serveroutput on

-------------------------------------
---          Update schema        ---
-------------------------------------

---           WEB USERS           ---

CREATE TABLE ACS_OWN.WEB_USERS(
    USER_ID         NUMBER        NOT NULL
        CONSTRAINT "WEB_USERS_PK"
                    PRIMARY KEY,
    USERNAME        VARCHAR2(32)                       NOT NULL,
    PASSWORD        VARCHAR2(64)                       NOT NULL,
    USR_EXP_DATE    DATE          DEFAULT null,
    INSERTION_DATE  DATE          DEFAULT CURRENT_DATE NOT NULL,
    CHECK_PASS_EXP  VARCHAR2(1)   DEFAULT 'N'          NOT NULL,
    PASS_EXP_DATE   DATE          DEFAULT CURRENT_DATE,
    LAST_LOGIN_DATE DATE          DEFAULT null,
    LAST_LOGIN_IP   VARCHAR2(128) DEFAULT null
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_USERS.USER_ID IS 'User identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.USERNAME IS 'Username' 
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.PASSWORD IS 'Password'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.USR_EXP_DATE IS 'Account expiration date'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.INSERTION_DATE IS 'Account insertion date'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.CHECK_PASS_EXP IS 'Check password expiration (Y) True (N) false'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.PASS_EXP_DATE IS 'Password expiration date'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.LAST_LOGIN_DATE IS 'User last authentication'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS.LAST_LOGIN_IP IS 'User last authentication ip address' 
/
---         WEB USERS INFO       ---

CREATE TABLE ACS_OWN.WEB_USERS_INFO
(
    USER_INFO_ID    NUMBER       NOT NULL
        CONSTRAINT "WEB_USERS_INFO_PK"
                    PRIMARY KEY,
    USER_ID         NUMBER
        CONSTRAINT WEB_USERS_INFO_USERS_FK
            REFERENCES ACS_OWN.WEB_USERS
                ON DELETE CASCADE,
    NAME            VARCHAR2(64),
    LASTNAME        VARCHAR2(64),
    PHONE           VARCHAR2(15),
    ADDRESS         VARCHAR2(100),
    EMAIL           VARCHAR2(32),
    ACC_STATUS      VARCHAR2(20) DEFAULT 'ATTIVO' NOT NULL,
    ACC_STATUS_DESC VARCHAR2(64)
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.USER_INFO_ID           IS 'Information identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.USER_ID                IS 'User identifier associated'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.NAME                   IS 'User name'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.LASTNAME               IS 'User lastname'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.PHONE                  IS 'User phone'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.ADDRESS                IS 'User addredd'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.EMAIL                  IS 'User email address'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.ACC_STATUS             IS 'Account status'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_INFO.ACC_STATUS_DESC        IS 'Status description'
/

---         WEB PROFILES        ---

CREATE TABLE ACS_OWN.WEB_PROFILES
(
    PROF_DESC  VARCHAR2(64) NOT NULL,
    PROFILE_ID NUMBER       NOT NULL
        CONSTRAINT WEB_PROFILES_ID_PK
            PRIMARY KEY
)
TABLESPACE ACS_OWN_DATA 
/

CREATE UNIQUE INDEX ACS_OWN.WEB_PROFILES_PROF_ID_PROF_DESC_UINDEX
    on WEB_PROFILES (PROFILE_ID,PROF_DESC)
/

COMMENT ON COLUMN ACS_OWN.WEB_PROFILES.PROFILE_ID IS 'Profile identifier'
/

COMMENT ON COLUMN ACS_OWN.WEB_PROFILES.PROF_DESC IS 'Profile description'
/




---       WEB USERS PROFILE      ---

create table ACS_OWN.WEB_USERS_PROFILES
(
    USER_ID    NUMBER not null
        CONSTRAINT WEB_USERS_PROFILES_USERS
            REFERENCES ACS_OWN.WEB_USERS,
    PROFILE_ID NUMBER
        CONSTRAINT WEB_USERS_PROFILES_PROFILES_FK
            REFERENCES ACS_OWN.WEB_PROFILES
                ON DELETE CASCADE
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_USERS_PROFILES.USER_ID IS 'User identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_USERS_PROFILES.PROFILE_ID IS 'Profile identifier'
/


---          WEB PAGES          ---

CREATE TABLE ACS_OWN.WEB_PAGES
(
    PAGE_ID        NUMBER NOT NULL
        CONSTRAINT "WEB_PAGES_PK"
                   PRIMARY KEY,
    PAGE_NAME      VARCHAR2(32),
    CUSTOMER       VARCHAR2(128),
    PAGE_DESC      VARCHAR2(128),
    PARENT_PAGE_ID NUMBER DEFAULT NULL
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_PAGES.PAGE_ID IS 'Page identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_PAGES.PAGE_NAME IS 'the page name'
/
COMMENT ON COLUMN ACS_OWN.WEB_PAGES.CUSTOMER IS 'Customer'
/
COMMENT ON COLUMN ACS_OWN.WEB_PAGES.PAGE_DESC IS 'Page description'
/

---      WEB PROFILES PAGES      ---

create table ACS_OWN.WEB_PROFILES_PAGES
(
    PROFILE_ID NUMBER
        CONSTRAINT WEB_PROFILES_PAGES_PROFILES_FK
            REFERENCES ACS_OWN.WEB_PROFILES,
    PAGE_ID    NUMBER
        CONSTRAINT WEB_PROFILES_PAGES_PAGES_FK
            REFERENCES ACS_OWN.WEB_PAGES
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_PROFILES_PAGES.PROFILE_ID IS 'Profile identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_PROFILES_PAGES.PAGE_ID IS 'Page identifier'
/

---          WEB TOKEN          ---

CREATE TABLE ACS_OWN.WEB_TOKEN
(
  JWT varchar2(1000) NOT NULL,
  STATUS varchar2(20) DEFAULT 'ATTIVO',
  ACCESS_TOKEN varchar2(1000),
  USERNAME varchar2(32),
  LAST_LOGIN_DATE DATE DEFAULT CURRENT_DATE
)
TABLESPACE ACS_OWN_DATA 
/


COMMENT ON COLUMN ACS_OWN.WEB_TOKEN.JWT             IS 'Access token'
/
COMMENT ON COLUMN ACS_OWN.WEB_TOKEN.STATUS          IS 'Account status'
/
COMMENT ON COLUMN ACS_OWN.WEB_TOKEN.ACCESS_TOKEN    IS 'External Access token'
/
COMMENT ON COLUMN ACS_OWN.WEB_TOKEN.USERNAME        IS 'Account username'
/
COMMENT ON COLUMN ACS_OWN.WEB_TOKEN.LAST_LOGIN_DATE IS 'Last authentication date'
/

---          WEB FILES          ---

create table ACS_OWN.WEB_FILES
(
    USERNAME       VARCHAR2(100)                                               NOT NULL,
    FILE_ID        NUMBER                                                      NOT NULL
        CONSTRAINT "WEB_FILES_PK"
            PRIMARY KEY,
    STATUS         VARCHAR2(20) DEFAULT 'PENDING',
    FILE_RECORDS   NUMBER       DEFAULT 0                                      NOT NULL,
    INSERTION_DATE DATE         DEFAULT CURRENT_DATE,
    LAST_UPDATE    DATE         DEFAULT CURRENT_DATE,
    FILE_PATH      VARCHAR2(500),
    FILE_NAME      VARCHAR2(100),
    FILE_TYPE      VARCHAR2(10) DEFAULT NULL,
    COUNTER        NUMBER DEFAULT 0
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_FILES.USERNAME           IS 'Username'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.FILE_ID            IS 'File identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.STATUS             IS 'File status'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.FILE_RECORDS       IS 'File records number'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.INSERTION_DATE     IS 'File insertion date'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.LAST_UPDATE        IS 'File last update date'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.FILE_PATH          IS 'File path'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.FILE_NAME          IS 'File name'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.FILE_TYPE          IS 'File type'
/
COMMENT ON COLUMN ACS_OWN.WEB_FILES.COUNTER            IS 'File download counter'
/
---      WEB CONSOLE LOG      ---


CREATE TABLE ACS_OWN.WEB_CONSOLE_LOG
(
    LOG_ID         NUMBER       NOT NULL
        CONSTRAINT WEB_CONSOLE_LOG_PK
            PRIMARY KEY,
    USERNAME       VARCHAR2(32),
    ACTION         VARCHAR2(20),
    DESCRIPTION    VARCHAR2(2048),
    INSERTION_DATE DATE         DEFAULT CURRENT_DATE,
    IP_ADDRESS     VARCHAR2(32),
    TOUCHED_TABLE  VARCHAR2(64) DEFAULT NULL
)
TABLESPACE ACS_OWN_DATA 
/

COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.LOG_ID       IS 'Log identifier'
/
COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.USERNAME     IS 'Username'
/
COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.ACTION       IS 'Action'
/
COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.DESCRIPTION    IS 'Log report'
/
COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.INSERTION_DATE   IS 'Log insertion date'
/
COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.IP_ADDRESS     IS 'User authentication ip address'
/
COMMENT ON COLUMN ACS_OWN.WEB_CONSOLE_LOG.TOUCHED_TABLE  IS 'Touched table'
/



-------------------------------------
---             Commit            ---
-------------------------------------

-- COMMIT

-------------------------------------
---              End              ---
-------------------------------------

spool off