/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/
set time on
set timing on
set echo on
set head off

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

--ACCEPT USER_OWNER PROMPT 'INSERT SCHEMA OWNER ' DEFAULT ACS20 ;
--ACCEPT TBS_DATA PROMPT 'INSERT TABLSPACE NAME FOR DATA ' DEFAULT USERS ;
--ACCEPT TBS_INDX PROMPT 'INSERT TABLSPACE NAME FOR INDEXES ' DEFAULT INDX;

REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';

REM*************************
REM* Change the spool name
REM*************************

col DATASPOOL noprint new_value NOME_REPORT

select '01_DT08_ACFS_3DS_ACS_1.0.2.18.20221219_MEPS_v1_ROLLBACK' DATASPOOL
  from dual ;

spool       &&NOME_REPORT..log

SELECT user FROM dual;

SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;

set serveroutput on
------------------------------------
--- Aggiornamento schema DB CORE --- 
------------------------------------
 
 
DROP TABLE ACS_OWN.TRANSACTION_ERROR CASCADE CONSTRAINTS;

DROP SEQUENCE ACS_OWN.SQ_TRANSACTION_ERROR_ID;


----------------------------------
--- Aggiornamento dati DB CORE --- 
----------------------------------
--commit;
------------
--- Fine --- 
------------
---spool off off
SPOOL OFF;
