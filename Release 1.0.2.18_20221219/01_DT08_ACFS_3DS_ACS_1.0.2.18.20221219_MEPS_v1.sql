/*****************************************************************
****                                                          ****
****    ACS 2.0 CORE SCHEMA                                   ****
****                                                          ****
*****************************************************************/
set time on
set timing on
set echo on
set head off

WHENEVER SQLERROR EXIT FAILURE ROLLBACK;

--ACCEPT USER_OWNER PROMPT 'INSERT SCHEMA OWNER ' DEFAULT ACS20 ;
--ACCEPT TBS_DATA PROMPT 'INSERT TABLSPACE NAME FOR DATA ' DEFAULT USERS ;
--ACCEPT TBS_INDX PROMPT 'INSERT TABLSPACE NAME FOR INDEXES ' DEFAULT INDX;

REM*alter session set nls_date_format = 'YYYY/MM/DD HH24:MI:SS';

REM*************************
REM* Change the spool name
REM*************************

col DATASPOOL noprint new_value NOME_REPORT

select '01_DT08_ACFS_3DS_ACS_1.0.2.18.20221219_MEPS_v1' DATASPOOL
  from dual ;

spool       &&NOME_REPORT..log

SELECT user FROM dual;

SELECT global_name || '  ' || TO_CHAR(CURRENT_DATE, 'YYYY/MM/DD HH24:MM:SS')FROM global_name;

set serveroutput on

------------------------------------
--- Aggiornamento schema DB CORE --- 
------------------------------------

CREATE SEQUENCE ACS_OWN.SQ_TRANSACTION_ERROR_ID
  START WITH 1
  MAXVALUE 999999999
  MINVALUE 1
  CYCLE
  CACHE 2000;
   

CREATE TABLE ACS_OWN.TRANSACTION_ERROR
  (
    TRANSACTION_ERROR_ID NUMBER(9,0) NOT NULL PRIMARY KEY,
    TRID                 NUMBER(9,0),
    TYPE                 VARCHAR2(1),
    MESSAGE_TYPE         VARCHAR2(4),
    DT                   DATE DEFAULT CURRENT_DATE,
    ERROR_CODE           VARCHAR2(20),
    ERROR_DESCRIPTION    VARCHAR2(256),
    ERROR_DETAIL         VARCHAR2(256),
    EXTRA                VARCHAR2(1000)
  ) TABLESPACE ACS_OWN_DATA ;
 
 
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.TRID IS 'Transaction unique identifier';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.TYPE IS 'Error type (E) Erro (O) Other';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.MESSAGE_TYPE IS 'Message type than gone in error';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.DT IS 'Insertion date';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.ERROR_CODE IS 'Error code';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.ERROR_DESCRIPTION IS 'Error description';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.ERROR_DETAIL IS 'Error detail';
  COMMENT ON COLUMN ACS_OWN.TRANSACTION_ERROR.EXTRA IS 'Other informations';
    
 
  CREATE INDEX ACS_OWN.TRANSACTION_ERROR_TRID_IDX ON ACS_OWN.TRANSACTION_ERROR(TRID) TABLESPACE ACS_OWN_INDEX ONLINE;
    
  CREATE INDEX ACS_OWN.TRANSACTION_ERROR_ERROR_CODE_IDX ON ACS_OWN.TRANSACTION_ERROR(ERROR_CODE) TABLESPACE ACS_OWN_INDEX ONLINE;

  grant select,insert,update,delete on ACS_OWN.TRANSACTION_ERROR to "ACS_OWN_RW";

	 

----------------------------------
--- Aggiornamento dati DB CORE --- 
----------------------------------

--commit;

------------
--- Fine --- 
------------

spool off